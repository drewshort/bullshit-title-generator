# Bullshit Title Generator

## Building

```bash
cargo build
```

## Installing

```bash
cargo install --path .
```

## Usage

```bash
$ bullshit-title-generator --help
bullshit-title-generator 0.2.0
Generate bullshit job titles

USAGE:
    bullshit-title-generator [OPTIONS]

OPTIONS:
    -c, --count <COUNT>    The number of titles to generate [default: 1]
    -h, --help             Print help information
    -V, --version          Print version information
```

## Example

```bash
$ bullshit-title-generator -c 5
Central Optimization Agent
Forward Response Coordinator
Dynamic Communications Planner
Legacy Team Developer
Global Tactics Manager
```

## Contributing

Contributions are not currently being actively considered, but merge requests may be reviewed


## License

bullshit-job-titles is licensed under the Apache V2 open source license. The full license is available at [LICENSE](./LICENSE)